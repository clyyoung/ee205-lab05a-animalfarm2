///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   16 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string whereFound, enum Color newColor, enum Gender newGender ) {
   gender = newGender;                 /// Get from the constructor... not all palila are the same gender (this is a has-a relationship)
   species = "Loxioides bailleui";     /// Hardcode this... all palila are the same species (this is a is-a relationship)
   featherColor = newColor;            /// A has-a relationship, so it comes through the constructor
   isMigratory = false;                /// An is-a relationship.  Every palila is not migratory.
   found = whereFound;                 /// Get from the constructor... not all palila are from the same place
}

/// Print our Palila and where found first... then print whatever information Bird holds.
void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << found << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm

